#!/bin/bash

# add apt source list
if [[ -f /etc/apt/sources.list.d/docker.list ]]; then
    echo "Apt source list for docker already added"
else
    echo "Uninstall old docker versions ..."
    sudo apt remove docker docker-engine docker.io containerd runc

    # add key
    if apt-key list | grep -q c; then
        echo "Docker key already added"
    else
        echo "Adding docker key ..."
        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    fi

    echo "Adding apt source list for docker ..."
    echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(. /etc/os-release; echo "$UBUNTU_CODENAME") stable" | sudo tee /etc/apt/sources.list.d/docker.list
fi

# "apt update && apt install docker-ce" was executed later
