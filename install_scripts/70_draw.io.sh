#!/bin/bash

# include functions
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
. "$DIR/../install.sh" --include-functions-only

if ! [[ -d ${HOME}/.config/draw.io ]]; then
    echoColored "Installing draw.io ..."
    snap install draw.io
else
    echo "draw.io is already installed: $(draw.io --version)"
fi
