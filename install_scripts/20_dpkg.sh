#!/bin/bash

# include functions
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
. "$DIR/../install.sh" --include-functions-only

# Reconfigure davfs2 to enable to use davfs under unprivileged users
if ! [[ -n $(find /usr/sbin/mount.davfs -perm /4000) ]]; then
    echoHighlight "Reconfigure davfs2 to enable to use davfs under unprivileged users"
    sudo dpkg-reconfigure davfs2
else
    echo "Unprivileged users are already able to use davfs"
fi
