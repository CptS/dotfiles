# .dotfiles

## Run

```bash
bash -c "`curl -fsSL https://gitlab.com/CptS/dotfiles/raw/master/install.sh`"
```

## TODOs

### Install

- Kubernetes / Minikube & Helm
- php & composer
- VirtualBox
- Web-Radio-Player (which one? Sayonara? Is Still required in latest ubuntu )
- Optional job software:
    - VPN: "Cisco AnyConnect"  
      `sudo apt install network-manager-openconnect network-manager-openconnect-gnome`
    - Citrix Receiver
    - Open eCard & PersoSim

### Config (dotfiles)

- Ecosia as search engine in firefox and chrome
- Week number in calendar
- vim
- firefox
    - Addon: KeePassXC-Browser
    - privacy filter / ad blocker for all tabs (not only private)
    - startpage: reopen tabs
- git config (especcially name & email; different settings for job!)
- ssh (different keys for job --> put the keys in the keepass database)
- VirtualBox
    - IE11 Image with restore point
- Web-Radio: BR-Klassik - http://streams.br.de/br-klassik_2.m3u
- Optional job software:
    - /etc/hosts
      ```
      192.168.145.82  intranet.muc.hd.local
      192.168.145.64  git.muc.hd.local
      ```
    - VPN: "Cisco AnyConnect"  
      - Gateway: `vpn.h-d-gmbh.de`
      - everything else empty / not selected 
    - Citrix Receiver settings (especially shared directories)  
      `/opt/Citrix/ICAClient/util/configmgr` - Dateizugriff
        - /home/reichardt/Schreibtisch/citrix-exchange (J:) Lesen/Schreiben
        - /home/reichardt/Projects (P:) Lesen/Schreiben
    - Custom Scripts: http://192.168.145.64/J.Reichardt/scripts
    - Auto Backup (ubuntu)
