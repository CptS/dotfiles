#!/bin/bash

# include functions
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
. "$DIR/../install.sh" --include-functions-only

# add user to docker group
USER_TO_ADD="$USER"
if ! groups | grep -q docker; then
    echoColored "Adding user \"$USER_TO_ADD\" to group \"docker\" ..."
    sudo groupadd docker
    sudo usermod -a -G docker ${USER_TO_ADD}

    # Fix file permissions
    sudo chown "$USER_TO_ADD":"$USER_TO_ADD" "$HOME/.docker" -R
    sudo chmod g+rwx "$HOME/.docker" -R

    echoColored "👤 New group \"docker\" was added to user \"$USER_TO_ADD\"."
else
    echo "👤 User \"$USER_TO_ADD\" is already a member of \"docker\""
fi
