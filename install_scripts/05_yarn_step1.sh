#!/bin/bash

# add apt source list
if [[ -f /etc/apt/sources.list.d/yarn.list ]]; then
    echo "Apt source list for yarn already added"
else
    # add key
    if apt-key list | grep -q yarn; then
        echo "Yarn key already added"
    else
        echo "Adding yarn key ..."
        curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
    fi

    echo "Adding apt source list for yarn ..."
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
fi

# "apt update && apt install --no-install-recommends yarn" was executed after npm & node.js was installed
