#!/bin/bash

# include functions
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
. "$DIR/../install.sh" --include-functions-only

CTOP_VERSION="0.7.3"

if ! [[ -f /usr/local/bin/ctop ]]; then
    echoColored "Installing ctop ..."
    sudo wget https://github.com/bcicen/ctop/releases/download/v${CTOP_VERSION}/ctop-${CTOP_VERSION}-linux-amd64 -O /usr/local/bin/ctop
    sudo chmod +x /usr/local/bin/ctop
else
    echo "ctop is already installed: $(ctop -v)"
fi

