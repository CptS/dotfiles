#!/bin/bash

TOOLBOX_VERSION="1.17.6856"
BASE_FILENAME="jetbrains-toolbox-${TOOLBOX_VERSION}"
DOWNLOAD_URL="https://download.jetbrains.com/toolbox/${BASE_FILENAME}.tar.gz"
DOWNLOAD_TEMP_FILE="/tmp/${BASE_FILENAME}.tar.gz"
FINAL_INSTALL_LOCATION="$HOME/.local/share/JetBrains/Toolbox/bin/jetbrains-toolbox"

# include functions
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
. "$DIR/../install.sh" --include-functions-only

areYouReady() {
    while true; do
        read -p "Are you ready? " -n 1 READY_NOW
        case ${READY_NOW} in
            [Yy]* ) echo && break;;
            [Nn]* ) echo && echo "Ok, I'm waiting ..."; break;;
            * ) echo && echo "Wrong answer! Please type \"y\" or \"n\".";;
        esac
    done
}

if ! [[ -f ${FINAL_INSTALL_LOCATION} ]]; then
    if ! [[ -d /tmp/${BASE_FILENAME} ]]; then
        if ! [[ -f ${DOWNLOAD_TEMP_FILE} ]]; then
            wget -O ${DOWNLOAD_TEMP_FILE} ${DOWNLOAD_URL}
        else
            echo "Using already downloaded archive \"$DOWNLOAD_TEMP_FILE\""
        fi
        echo "Extracting downloaded tar.gz ..."
        sudo mkdir /tmp/${BASE_FILENAME}
        sudo chown ${USERNAME}. /tmp/${BASE_FILENAME}
        tar xfz ${DOWNLOAD_TEMP_FILE} -C /tmp/${BASE_FILENAME} --strip-components=1
    else
        echo "Using already extracted download files \"/tmp/${BASE_FILENAME}\""
    fi
    echoColored "────────────────────────────────────────────────────────────────────────────────"
    echoColored "  ℹ  You have to install IntelliJ IDEA by using the JetBrains Toolbox. Then"
    echoColored "     you're able to create a Desktop Entry and Command-line Launcher. Both could"
    echoColored "     be found in first-run dialog or later in the Tools menu of IntelliJ IDEA."
    while true; do
        read -p "Do you want to start the JetBrains Toolbox now? " -n 1 START_TOOLBOX_NOW
        case ${START_TOOLBOX_NOW} in
            [Yy]* ) echo && /tmp/${BASE_FILENAME}/jetbrains-toolbox 2>/dev/null; echo "The Toolbox should start in a few seconds ..."; areYouReady; break;;
            [Nn]* ) echo && echo "Ok, not now. You can do this later by executing the following command:"; echo ""; echo "    /tmp/${BASE_FILENAME}/jetbrains-toolbox"; echo ""; break;;
            * ) echo && echo "Wrong answer! Please type \"y\" or \"n\".";;
        esac
    done
else
    INSTALLED_VERSION=`${FINAL_INSTALL_LOCATION} -v 2>/dev/null`
    echo "JetBrains Toolbox already installed: $INSTALLED_VERSION"
fi
