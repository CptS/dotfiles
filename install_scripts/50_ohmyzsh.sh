#!/bin/bash

# include functions
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
. "$DIR/../install.sh" --include-functions-only

OH_MY_ZSH_DIR="${HOME}/.oh-my-zsh"
if ! [[ -d ${OH_MY_ZSH_DIR} ]]; then
    echoColored "Installing Oh My Zsh ..."
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

    echoColored "Installing Powerline Fonts ..."
    sudo apt install fonts-powerline
else
    echo "Oh My Zsh is already installed: $(git -C ${OH_MY_ZSH_DIR} log -1 --format=%cd)"
fi

ZSH_CUSTOM="${OH_MY_ZSH_DIR}/custom"
if ! [[ -f "$ZSH_CUSTOM/themes/spaceship.zsh-theme" ]]; then
    echoColored "Downloading theme 🚀 ⭐ Spaceship ZSH"
    git clone https://github.com/denysdovhan/spaceship-prompt.git "$ZSH_CUSTOM/themes/spaceship-prompt"
    ln -s "$ZSH_CUSTOM/themes/spaceship-prompt/spaceship.zsh-theme" "$ZSH_CUSTOM/themes/spaceship.zsh-theme"
fi

rm ${HOME}/.zshrc
ln -s ${HOME}/.dotfiles/config/.zshrc ~/.zshrc
