#!/bin/bash

# variables
PPAS="phoerious/keepassxc"
APPLICATIONS="vim git curl ffmpeg xdotool x11-utils libnotify-bin dos2unix zsh davfs2 keepassxc firefox"
APPLICATIONS="${APPLICATIONS} google-chrome-stable signal-desktop spotify-client vlc htop filezilla inkscape gimp"
APPLICATIONS="${APPLICATIONS} apt-transport-https ca-certificates curl software-properties-common docker-ce"
APPLICATIONS="${APPLICATIONS} gtk2-engines-murrine gtk2-engines-pixbuf"

# include functions
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
. "$DIR/../install.sh" --include-functions-only

# adding PPAs
echoHighlight "Adding PPAs ..."
for ppa in $PPAS; do
    if ! grep -q "^deb .*$ppa" /etc/apt/sources.list /etc/apt/sources.list.d/*; then
        echoColored "adding PPA $ppa"
        sudo add-apt-repository "ppa:$ppa" || exit
    else
        echo "PPA $ppa is already added"
    fi
done

# prepare
if [[ ${SKIP_UPDATE} == 0 ]]; then
    echoHighlight "Updating system ..."
    sudo apt update -y
    sudo apt upgrade -y
    sudo apt dist-upgrade -y
    echoColored "✅ System is now up to date"
fi

# install applications
echoHighlight "Installing applications ..."
sudo apt install ${APPLICATIONS} -y
echoColored "✅ Applications are installed"
