#!/bin/bash

# include functions
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
. "$DIR/../install.sh" --include-functions-only

if [[ -d ${HOME}/.yarn ]]; then
    echo "Yarn is already installed: $(yarn --version)"
else
    echoColored "Installing yarn ..."
    sudo apt install --no-install-recommends yarn
fi
