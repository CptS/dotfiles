#!/bin/bash

# add apt source list
if [[ -f /etc/apt/sources.list.d/spotify.list ]]; then
    echo "Apt source list for spotify already added"
else
    # add key
    if apt-key list | grep -q spotify; then
        echo "Spotify key already added"
    else
        echo "Adding spotify key ..."
        curl -sS https://download.spotify.com/debian/pubkey.gpg | sudo apt-key add -
    fi

    echo "Adding apt source list for spotify ..."
    echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list
fi

# "apt update && apt install spotify-client" was executed later
