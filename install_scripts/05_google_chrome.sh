#!/bin/bash

# add apt source list
if [[ -f /etc/apt/sources.list.d/google-chrome.list ]]; then
    echo "Apt source list for google chrome already added"
else
    # add key
    if apt-key list | grep -q google; then
        echo "Google key already added"
    else
        echo "Adding google key ..."
        wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
    fi

    echo "Adding apt source list for google chrome ..."
    echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | sudo tee /etc/apt/sources.list.d/google-chrome.list
fi

# "apt update && apt install google-chrome-stable" was executed later
