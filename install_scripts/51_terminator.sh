#!/bin/bash

# include functions
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
. "$DIR/../install.sh" --include-functions-only

TERMINATOR_CONFIGFILE="${HOME}/.config/terminator/config"

sudo apt install terminator -y
rm ${TERMINATOR_CONFIGFILE}
ln -s ${HOME}/.dotfiles/config/.config/terminator/config ${TERMINATOR_CONFIGFILE}
