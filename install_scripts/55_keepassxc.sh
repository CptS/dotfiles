#!/bin/bash

# include functions
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
. "$DIR/../install.sh" --include-functions-only


mountNow() {
    echoColored "Mounting /mnt/keypass now ..."
    su - ${USER_TO_ADD} -c 'mount /mnt/keypass'
}

initKeepass() {
    vim  ~/.davfs2/secrets
    mountNow
    echoHighlight "KeePass database is set up. Don't forget to copy the key file."
    echoColored "   (press enter to proceed)"
    read -n 1 -s -r
}

checkAutoMount() {
    if ! grep -q "mount /mnt/keypass" ~/.profile; then
        echoColored "Adding auto mount command to \"~/.profile\" ..."
        echo "" >> ~/.profile
        echo "# Mount keypass WebDAV share after login" >> ~/.profile
        echo "mount /mnt/keypass" >> ~/.profile
    else
        echo "Auto mount command already created in \"~/.profile\""
    fi
}


# add user to davfs2 group
USER_TO_ADD="$USER"
if ! groups | grep -q davfs2; then
    echoColored "Adding user \"$USER_TO_ADD\" to group \"davfs2\" ..."
    sudo usermod -a -G davfs2 ${USER_TO_ADD}
    echoColored "👤 New group \"davfs2\" was added to user \"$USER_TO_ADD\"."
else
    echo "👤 User \"$USER_TO_ADD\" is already a member of \"davfs2\""
fi

# create directory for webdav mount point
if ! [[ -d /mnt/keypass ]]; then
    echoColored "Creating directory /mnt/keypass ..."
    sudo mkdir /mnt/keypass
fi

# fstab entry
if ! grep -q "/mnt/keypass" /etc/fstab; then
    echoColored "Adding fstab entry for password database ..."
    sudo sh -c "echo 'http://jollyroger.duckdns.org:8666/keypass/cpts /mnt/keypass davfs user,noauto,file_mode=600,dir_mode=700 0 1' >> /etc/fstab"
else
    echo "Fstab entry for password database already existing"
fi

if ! mount | grep -q /mnt/keypass; then
    if ! grep -q "jollyroger" ~/.davfs2/secrets; then
        while true; do
            read -p "WebDAV secrets not found. Do you want to edit \" ~/.davfs2/secrets\" now? " -n 1 PROCEED_AS_USER
            case ${PROCEED_AS_USER} in
                [Yy]* ) echo && initKeepass; break;;
                [Nn]* ) echo && echoColored "Mointing \"/mnt/keypass\" skipped!" && exit;;
                * ) echo && echo "Wrong answer! Please type \"y\" or \"n\".";;
            esac
        done
    else
        mountNow
    fi
else
    echo "Directory \"/mnt/keypass\" already mounted"
fi

checkAutoMount
