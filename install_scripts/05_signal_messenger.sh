#!/bin/bash

# add apt source list
if [[ -f /etc/apt/sources.list.d/signal-xenial.list ]]; then
    echo "Apt source list for signal messenger already added"
else
    # add key
    if apt-key list | grep -q whispersystems; then
        echo "Signal messenger key already added"
    else
        echo "Adding signal messenger key ..."
        curl -s https://updates.signal.org/desktop/apt/keys.asc | sudo apt-key add -
    fi

    echo "Adding apt source list for signal messenger ..."
    echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list
fi

# "apt update && apt install signal-desktop" was executed later
