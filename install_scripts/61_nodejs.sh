#!/bin/bash

NVM_VERSION="v0.35.3"

# include functions
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
. "$DIR/../install.sh" --include-functions-only

makeNvmExecutableNow() {
    export NVM_DIR="$HOME/.nvm"
    [[ -s "$NVM_DIR/nvm.sh" ]] && \. "$NVM_DIR/nvm.sh"
}

if [[ -d ${HOME}/.nvm ]]; then
    makeNvmExecutableNow
    echo "Nvm is already installed: $(nvm --version)"
else
    echoColored "Installing nvm ..."
    curl -o- https://raw.githubusercontent.com/creationix/nvm/${NVM_VERSION}/install.sh | bash

    makeNvmExecutableNow

    echoColored "Installing node.js LTS ..."
    nvm install --lts
fi
