#!/bin/bash

JAVA_VERSION=11.0.7-open
MVN_VERSION=3.6.3

# include functions
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
. "$DIR/../install.sh" --include-functions-only

# install sdkman via curl
if ! [[ -d ~/.sdkman ]]; then
    echoColored "Installing SDKMAN! ..."
    curl -s "https://get.sdkman.io" | bash
    source "$HOME/.sdkman/bin/sdkman-init.sh"
fi

# make sure "sdk" function is executable
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$SDKMAN_DIR/bin/sdkman-init.sh" ]] && source "$SDKMAN_DIR/bin/sdkman-init.sh"

# populate "not_interactive" config
rm $SDKMAN_DIR/etc/config
ln -s ~/.dotfiles/config/.sdkman/etc/config_not_interactive $SDKMAN_DIR/etc/config

# constants
INSTALLED_PREFIX="[*+]"
DEFAULT_PREFIX="> [*+]"

# utility functions
installSdk() {
    LIST=`sdk list $1`
    if echo "$LIST" | grep -qe "$INSTALLED_PREFIX $2"; then
        echo "✅ $1 $2 already installed"
    else
        echoColored "Installing $1 $2 ..."
        sdk install $1 $2
    fi
}
installSdkAndDefault() {
    LIST=`sdk list $1`
    if echo "$LIST" | grep -qe "$DEFAULT_PREFIX $2"; then
        echo "✅ $1 $2 already set as default"
    else
        installSdk $1 $2
        echoColored "Setting $1 $2 as default ..."
        sdk default $1 $2
    fi
}

# update sdkman itself
if [[ ${SKIP_UPDATE} == 0 ]]; then
    sdk update
fi

# install some SDKs
installSdkAndDefault java $JAVA_VERSION
installSdkAndDefault maven $MVN_VERSION

# populate default config
rm $SDKMAN_DIR/etc/config
ln -s ~/.dotfiles/config/.sdkman/etc/config $SDKMAN_DIR/etc/config
