#!/bin/bash

REPO_NAME="dotfiles"
REPO_GIT_URL="git@gitlab.com:CptS/$REPO_NAME.git"
DOTFILE_DIR=".$REPO_NAME"
DOTFILE_PATH="$HOME/$DOTFILE_DIR"

# Colors
HC='\033[0;35m' # Highlight Color
NC='\033[0m' # No Color
FONT_BOLD=$(tput bold)
FONT_NORMAL=$(tput sgr0)

echoColored() {
    echo -e "${HC}$1${NC}"
}

echoHighlight() {
    echoColored "────────────────────────────────────────────────────────────────────────────────"
    echoColored "${FONT_BOLD}👉 $1${FONT_NORMAL}"
}

verifyUser() {
    while true; do
        read -p "Do you want to proceed? " -n 1 PROCEED_AS_USER
        case ${PROCEED_AS_USER} in
            [Yy]* ) echo && echoColored "Hello $USER!"; break;;
            [Nn]* ) echo && echoColored "Good bye!" && exit;;
            * ) echo && echo "Wrong answer! Please type \"y\" or \"n\".";;
        esac
    done
}

runGit() {
    if ! [[ -x "$(command -v git)" ]]; then
        echoColored "Git command not found - it will be installed now ..."
        sudo apt update || { echoColored "Installing git failed - See error above."; exit 1; }
        sudo apt install git || { echoColored "Installing git failed - See error above."; exit 1; }
    fi
    echoColored "Running git $1 ..."
    git "$@" || { echoColored "Git command failed - See error above."; exit 1; }
}

FUNCTIONS_ONLY=${FUNCTIONS_ONLY:-0}
SKIP_UPDATE=${SKIP_UPDATE:-0}
parseParams() {
    for i in "$@"; do
        case $i in
            --include-functions-only) FUNCTIONS_ONLY=1;;
            --skip-update) SKIP_UPDATE=1;;
            *) echoColored "Unknown parameter \"${i#*=}\"!" && exit 1;;
        esac
    done
}

runScript() {
    # avoid running as root
    if [[ "$EUID" -eq "0" ]]; then
        echoHighlight "Don't run this script as root! Root privileges will be asked if necessary."
        exit 1
    fi

    # start script
    echoColored "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓"
    echoColored "┃ Starting .dotfiles initialization ... ┃"
    echoColored "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛"

    echo "You are running this script as user \"$USER\" with home directory \"$HOME\"."
    verifyUser

    # get sudo privileges now
    echoColored "────────────────────────────────────────────────────────────────────────────────"
    echoColored "  ℹ  Sudo privileges are required for some installation steps."
    echoColored "     You would be asked for your password multiple times."
    sudo echo

    DIR=${DOTFILE_PATH}
    if [[ ${SKIP_UPDATE} == 0 ]]; then
        if [[ -d "$DOTFILE_PATH" ]]; then
            echoHighlight "\"$DOTFILE_PATH\" already exists - updating it ..."
            cd ${DOTFILE_PATH}
            runGit pull
        else
            echoHighlight "Cloning repository to \"$DOTFILE_PATH\" ..."
            runGit clone ${REPO_GIT_URL} ${DOTFILE_PATH}
            cd ${DOTFILE_PATH}
        fi
    else
        DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
    fi

    for f in ${DIR}/install_scripts/*.sh; do
        echoColored "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
        echoColored "${FONT_BOLD}📦 Running install script $(basename ${f}) ...${FONT_NORMAL}"
        source "$f" || exit
    done
}

parseParams $@
if [[ ${FUNCTIONS_ONLY} == 0 ]]; then
    runScript
fi
