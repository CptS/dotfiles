#!/bin/bash

# include functions
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
. "$DIR/../install.sh" --include-functions-only

FRANZ_VERSION="5.5.0"
FRANZ_DEB_FILE="franz_${FRANZ_VERSION}_amd64.deb"
FRANZ_DOWNLOAD_URL="https://github.com/meetfranz/franz/releases/download/v${FRANZ_VERSION}/${FRANZ_DEB_FILE}"

if ! [[ -d ${HOME}/.config/Franz ]]; then
    echoColored "Installing Franz ..."
    sudo wget ${FRANZ_DOWNLOAD_URL} -O /tmp/${FRANZ_DEB_FILE}
    sudo dpkg -i /tmp/${FRANZ_DEB_FILE}
    sudo rm -f /tmp/${FRANZ_DEB_FILE}
else
    echo "Franz is already installed: $(dpkg -s franz | grep Version)"
fi
