#!/bin/bash

SIZE="${1:-1920x1080}"

BASE_URL="https://www.smashingmagazine.com"
OVERVIEW_PAGE="${BASE_URL}/category/wallpapers"
MONTH=`LC_ALL=en_US.utf8 date +%B`
DATE=`date +%Y-%m-%d`
TEMP_OVERVIEW_FILE="/tmp/smashing_magazine_wallpaper_overview_${MONTH}_${DATE}.html"
TEMP_MONTH_FILE="/tmp/smashing_magazine_wallpaper_${MONTH}_${DATE}.html"
TARGET_DIR="/usr/share/backgrounds/smashing-magazine"

searchInHtml() {
    cat $1 | dos2unix | tr '\n' '\r' |  perl -pe "s|.*?$2.*|\1|i"
}
if ! [[ -f ${TEMP_MONTH_FILE} ]]; then
    if ! [[ -f ${TEMP_OVERVIEW_FILE} ]]; then
        echo "Downloading overview page ${OVERVIEW_PAGE}"
        curl ${OVERVIEW_PAGE} -o ${TEMP_OVERVIEW_FILE}
    else
        echo "Using cached overview page"
    fi

    PAGE_PATH=`searchInHtml ${TEMP_OVERVIEW_FILE} "tilted-featured-article__title.*?<a\s.*?href=\"?([^\">]*)\"?(?:\s+.*)*>"`

    if ! [[ $PAGE_PATH == /* ]]; then
        echo "Can't find path to month page in overview html!"
        exit 1;
    fi

    MONTH_PAGE="${BASE_URL}${PAGE_PATH}"
    echo "Downloading page for latest month: ${MONTH_PAGE}"
    curl ${MONTH_PAGE} -o ${TEMP_MONTH_FILE}
else
    echo "Using cached month page"
fi

DOWNLOAD_URL=`searchInHtml ${TEMP_MONTH_FILE} "\s+href=\"?([^\">]*)\"?(?:\s+[^>]*)*>\s*${SIZE}\s*</a>"`

echo "Downloading image: ${DOWNLOAD_URL}"
TARGET_FILE="${TARGET_DIR}/$(basename ${DOWNLOAD_URL})"
sudo mkdir -p ${TARGET_DIR}
sudo curl ${DOWNLOAD_URL} -o ${TARGET_FILE}

echo "Setting background image: ${TARGET_FILE}"
# TODO: This doesn't work in Ubuntu but works fine in Mint/Cinnamon so it's ok for now but maybe there is a generic solution?
gconftool-2 --type string --set /desktop/gnome/background/picture_filename ${TARGET_FILE};
