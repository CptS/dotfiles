#!/bin/bash

# include functions
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
. "$DIR/../install.sh" --include-functions-only

TEAMS_VERSION="1.3.00.5153"
TEAMS_DEB_FILE="teams_${TEAMS_VERSION}_amd64.deb"
TEAMS_DOWNLOAD_URL="https://packages.microsoft.com/repos/ms-teams/pool/main/t/teams/${TEAMS_DEB_FILE}"

if ! [[ -d ${HOME}/.config/teams ]]; then
    echoColored "Installing Microsoft Teams ..."
    sudo wget ${TEAMS_DOWNLOAD_URL} -O /tmp/${TEAMS_DEB_FILE}
    sudo dpkg -i /tmp/${TEAMS_DEB_FILE}
    sudo rm -f /tmp/${TEAMS_DEB_FILE}
else
    echo "Microsoft Teams is already installed: $(dpkg -s teams | grep Version)"
fi
